const express = require("express")
const favicon = require("serve-favicon")
const path = require("path")

const app = express()
const default_port = 3001


app.set("port", process.env.PORT || default_port)

// custom x-powered-by
app.use(function (req, res, next) {
 res.header("x-powered-by", "cyrus")
 next()
})

app.set("views", path.join(__dirname, "view"))
app.set("view engine", "jade")

app.use("/", require("./route/map"))

app.use(favicon(path.join(__dirname, "public", "favicon.ico")))
const oneDay = 86400000
app.use(express.static(path.join(__dirname, "public"), { maxAge: oneDay }))

app.listen(app.get("port"), (err) => {
 if (err) {
  return console.log("something bad happened", err)
 }

 console.log(`server is listening on ${app.get("port")}`)
})

app.use(function(req, res, next) {
 var err = new Error("Not Found")
 err.status = 404
 next(err)
})

app.use(function(err, req, res, next) {
 res.status(err.status || 500)
 res.render("error", {
  message: err.message,
  error: {}
 })
})


module.exports = app


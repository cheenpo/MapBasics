
$(function() {
  var OD_PAIRS = [
    ["NRT", "JFK"],
    ["SFO", "NRT"],
    ["LAX", "HNL"],
    ["HNL", "NRT"],
    ["CDG", "JFK"],
    ["NRT", "SYD"],
    ["FCO", "PEK"],
    ["LHR", "PVG"],
    ["NRT", "ARN"],
    ["LAX", "JFK"],
    ["NRT", "DEL"],
    ["DFW", "GRU"],
    ["MAD", "ATL"],
    ["ORD", "CAI"],
    ["HKG", "CDG"],
    ["LAS", "CDG"],
    ["NRT", "SVO"],
    ["DEN", "HNL"],
    ["ORD", "LAX"],
    ["SIN", "SEA"],
    ["SYD", "PEK"],
    ["CAI", "CPT"],
    ["CUN", "JFK"],
    ["ORD", "JFK"],
    ["LHR", "BOM"],
    ["LAX", "MEX"],
    ["LHR", "CPT"],
    ["PVG", "CGK"],
    ["SYD", "BOM"],
    ["JFK", "CPT"],
    ["MAD", "GRU"],
    ["EZE", "FCO"],
    ["DEL", "DXB"],
    ["DXB", "NRT"],
    ["GRU", "MIA"],
    ["SVO", "PEK"],
    ["YYZ", "ARN"],
    ["LHR", "YYC"],
    ["HNL", "SEA"],
    ["JFK", "EZE"],
    ["EZE", "LAX"],
    ["CAI", "HKG"],
    ["SVO", "SIN"],
    ["IST", "MCO"],
    ["MCO", "LAX"],
    ["FRA", "LAS"],
    ["ORD", "FRA"],
    ["MAD", "JFK"]
  ];

  var currentWidth = $('#map').width();
  var width = 938;
  var height = 620;

  var projection = d3.geo
                     .mercator()
                     .scale(150)
                     .translate([width / 2, height / 1.41]);

  var path = d3.geo
               .path()
               .pointRadius(2)
               .projection(projection);
  
  var svg = d3.select("#map")
              .append("svg")
              .attr("preserveAspectRatio", "xMidYMid")
              .attr("viewBox", "0 0 " + width + " " + height)
              .attr("width", currentWidth)
              .attr("height", currentWidth * height / width);

  var locationsMap = {};
 
  function transition(traffic, route) {
    var l = route.node().getTotalLength();
    traffic.transition()
        .duration(l * 15)  // this is the speed of movement
        .attrTween("transform", delta(traffic, route.node()))
        .each("end", function() { route.remove(); })
        .remove();
  }
  
  function delta(traffic, path) {
    var l = path.getTotalLength();
    var traffic = traffic;
    return function(i) {
      return function(t) {
        var p = path.getPointAtLength(t * l);

        var t2 = Math.min(t + 0.05, 1);
        var p2 = path.getPointAtLength(t2 * l);

        var x = p2.x - p.x;
        var y = p2.y - p.y;
        var r = 90 - Math.atan2(-y, x) * 180 / Math.PI;

        var s = Math.min(Math.sin(Math.PI * t) * 0.7, 0.3);

        return "translate(" + p.x + "," + p.y + ") scale(" + s + ") rotate(" + r + ")";
      }
    }
  }


  function fly(origin, destination) {
    var paths = svg.append("path")
               .datum({type: "LineString", coordinates: [origin, destination]})
               .attr("class", "route");

    var route = svg.append("path")
                   .datum({type: "LineString", coordinates: [locationsMap[origin], locationsMap[destination]]})
                   .attr("class", "route");

    var traffic = svg.append("circle")
                .attr("cx", 30)
                .attr("cy", 30)
                .attr("r", 10)
                .attr("fill", "#9842f4")

    transition(traffic, route);
  }

  function loaded(error, countries, locations) {
    svg.append("g")
       .attr("class", "countries")
       .selectAll("path")
       .data(topojson.feature(countries, countries.objects.countries).features)
       .enter()
       .append("path")
       .attr("d", path);

    svg.append("g")
       .attr("class", "locations")
       .selectAll("path")
       .data(topojson.feature(locations, locations.objects.locations).features)
       .enter()
       .append("path")
       .attr("id", function(d) {return d.id;})
       .on("mouseover", function(d,i) {
         this.style.cursor='pointer';
         document.getElementById("d_data").innerHTML = d.id;
        })
       .attr("d", path);

    var geos = topojson.feature(locations, locations.objects.locations).features;
    for (i in geos) {
      locationsMap[geos[i].id] = geos[i].geometry.coordinates;
    }

    var i = 0;
    setInterval(function() {
      if (i > OD_PAIRS.length - 1) {
        i = 0;
      }
      var od = OD_PAIRS[i];
      fly(od[0], od[1]);
      i++;
    }, 75);
  }

  queue().defer(d3.json, "/json/countries.topo.json")
         .defer(d3.json, "/json/locations.topo.json")
         .await(loaded);

  $(window).resize(function() {
    currentWidth = $("#map").width();
    svg.attr("width", currentWidth);
    svg.attr("height", currentWidth * height / width);
  });
});

